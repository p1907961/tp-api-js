
const linkChecker = {
    testLink : async (baseUrl, callBackDisplayResult,callbackError) => {
        const result = {
            'ok' : 0,
            'ko' : 0,
            'unknown' : 0
        }
        // Test si BaseUrl est un lien HTTP valide
        if(baseUrl.match(/^http[s]{0,1}:\/\/(?:.*)$/)) {
            //On récupère le contenu html de la page
            linkChecker.getContentUrl(baseUrl, (htmlContent) => {
                //On récupère les urls du contenu html
                const urlsToCheck = linkChecker.extractUrlFromHtml(htmlContent, baseUrl)
                //Nombres de liens à checker
                const nbUrlsToCheck = urlsToCheck.length
                //Nombre de liens déjà checké
                let urlChecked = 0
                //Test de chaques liens
                urlsToCheck.forEach(async urlToCheck => {
                    // Validation d'un lien HTTP(S) correct
                    if (urlToCheck['href'].match(/^http[s]{0,1}:\/\/(?:.*)$/)) {
                        // Requete web vers la ressource
                        await fetch(urlToCheck['href'])
                            .then((response) => {
                                // check du status de la réponse Web
                                switch (response['status']) {
                                    case 200:
                                        // Ressource existante
                                        result['ok']++;
                                        break;
                                    case 404:
                                        // Ressource introuvable
                                        result['ko']++;
                                        break;
                                }
                            }).catch((e) => {
                                // Ressource inaccessible
                                result['ko']++;
                            })
                    } else {
                        // Lien de la ressource invalide
                        result['unknown']++;
                    }
                    //Lien testé
                    urlChecked++
                    //Si on a testé tous les liens on peut afficher les résultats
                    if (urlChecked === nbUrlsToCheck) {
                        callBackDisplayResult(result)
                    }
                })
            }, callbackError)
        }else{
            // Lien de baseUrl invalide, envoi d'une erreur
            callbackError(new Error('Url Mal Formée'))
        }
    },
    getContentUrl:  async (url,callback, callbackError)=>{
        //Appel Ajax
        fetch(url)
        .then((response)=>{
            // Exception sur les code HTTP 403, 404, 500
            switch(response['status']){
                case 403:
                case 404:
                case 500:
                    // Renvoi d'une erreur sur le callback destiné aux erreurs
                    callbackError(new Error('erreur HTTP ' + response['status']))
                    break;
                default:
                    // réponse HTTP
                    return response.text()
            }
        })
        .then(contentHtml=>{
            //Contenu de la réponse HTTP
            callback(contentHtml)
        }).catch((e)=>{
            // Erreur
        })
    },
    extractUrlFromHtml: (htmlCode,baseUrl)=>{
        const regex = "(src|href)\\s*=\\s*[\"']([^\"']*)[\"']";
        return [...htmlCode.matchAll(regex)].map((urlMatches)=>{
            return new URL(urlMatches[2], baseUrl)
        })
    },
} 